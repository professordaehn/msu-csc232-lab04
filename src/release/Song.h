/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Song.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 * @brief   Song specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef LAB04_SONG_H
#define LAB04_SONG_H

#include <string>

class Song {
private:
    std::string m_artist;
    std::string m_title;
public:
    Song(const std::string& artist, const std::string& title);
    std::string getArtist() const;
    std::string getTitle() const;
    virtual ~Song();
};


#endif //LAB04_SONG_H
