/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    Song.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 * @brief   Song implementation.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include "Song.h"

Song::Song(const std::string& artist, const std::string& title) :
        m_artist(artist), m_title(title) {
}

std::string Song::getArtist() const {
    return m_artist;
}

std::string Song::getTitle() const {
    return m_title;
}

Song::~Song() {
}
